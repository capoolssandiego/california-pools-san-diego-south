California Pools - San Diego (South)

California Pools is one of the largest pool builders in the country and brings award-winning custom pools to the San Diego area. As seen on Pool Kings, we offer expert design solutions and innovative construction techniques to provide our customers with the highest-quality pool or backyard anywhere.

Address: 4568 4th St, La Mesa, CA 91941, USA
Phone: 760-201-1466
Website: https://californiapools.com/locations/san-diego-south